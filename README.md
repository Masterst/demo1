# Facial Recognition

Un sistema de reconocimiento facial con su propia interfaz grafica capaz de guardar los rostros de personas para identificarlas despúes

Para descargar esto solo hara falta el siguiente comando:

sudo git clone "link del gitlab"


## actualizar el sistema

Primero debemos tener el sistema actualizado para que se pueda descargar todo sin problemas.

usamos el siguiente comando para actualizarlo:

"sudo apt update"

## Instalar VSCode

Instalaremos nuestro editor que en este caso sera el famoso Visual Studio Code

"sudo snap install --classic Code"


## Instalar paquetes

En la terminal de linux usamos el comando "apt install" o el que se use en la distribución que este usando e instalamos los siguientes paquetes:

"sudo apt install python3"

"sudo apt-get install python3-tk"

"sudo apt-get install cmake"

Tambien en el editor de VSCode instalamos la extension python para que pueda leer y ejecutar nuestro codigo

Para esto entramos al code, en la parte izquierda hay una opcion llamada "extensions", le damos click y buscamos "python", clickeamos en instalar y ya estaria.

## Instalar los modulos

En este caso usaremos el comando pip y pondremos lo siguiente:

"sudo pip install -r requeriments.txt"

Esto nos instalara todos los modulos que usaremos.


# Ejecutar el archivo main

El archivo que se debe ejecutar es el llamado "main.py"

Al ejecutarlo nos aparecera un error de un archivo incompatible en la carpeta "faces"
para solucionarlo basta con eliminar ese archivo, se puede usar el siguiente comando:

"rm .gitkeep"

## reconocimiento

El reconocimiento facial se activa cuando se le da al boton de iniciar video en la primera ventana, para que funcione primero habra que hacer una foto de una cara que se puede hacer con el boton registrarse.

El reconocimiento no funcionara si hay un archivo incompatible en la carpeta donde se guardan las caras.
